from django.urls import path

from . import views

app_name = 'jeugd'
urlpatterns = [
    path('', views.index, name='index'),
    path('lectures/', views.lectures, name='lectures'),
    path('lecture/', views.lecture, name='lecture'),
]
