from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, 'jeugd/index.html')


def lectures(request):
    return render(request, 'jeugd/lectures.html')


def lecture(request):
    return render(request, 'jeugd/lecture.html')
